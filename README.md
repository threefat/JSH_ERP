#jshERP完整开源版J2EE代码
* 很多人说jshERP是目前唯一完整开源的进销存系统
* 虽然目前只有进销存+财务的功能，但后面将会推出ERP的全部功能，大家一起努力吧
* 系统部署初始账号：jsh，密码：123456，部署如有问题请联系QQ：752718920，欢迎加入jshERP交流群，群号码：120725710
* 部署如遇到困难请参考Wiki文档  **https://gitee.com/jishenghua/JSH_ERP/wikis/pages** 

#开发初衷
* jshERP立志为中小企业提供开源好用的ERP软件，降低企业的信息化成本
* 个人开发者也可以使用jshERP进行二次开发，承接外包ERP项目
* 初学JAVA的小伙伴可以下载源代码来进行学习交流

#技术框架
* 核心框架：Spring Framework 3.0.5
* 视图框架：Struts 2.2.3
* 持久层框架：Hibernate 3.0
* 日志管理：Log4j 1.2.16
* JS框架：Jquery 1.8.0
* UI框架: EasyUI 1.3.5
* 桌面框架: HoorayOS
* 项目管理框架: Maven3.2.3

#开发环境
建议开发者使用以下环境，可以避免版本带来的问题
* IDE: MyEclipse8.5+/Eclipse4.4+
* DB: Mysql5.1
* JDK: JDK1.6+
* WEB: Tomcat6.0+
* Maven: Maven3.2.3+
* 为方便大家搭建开发环境，分享了下载地址  **http://pan.baidu.com/s/1nuKnlNV** 

#运行环境
* WEB服务器：Tomcat6.0+
* 数据库服务器：Mysql5.1
* JAVA平台: JRE1.6+
* 操作系统：Windows、Linux等

#开源说明
* 本系统100%开源，遵守Apache2.0协议

#系统美图
* 进销存模块
![输入图片说明](https://gitee.com/uploads/images/2017/0108/150544_853dcc2e_852955.png "进销存模块")
* 报表查询模块
![输入图片说明](https://gitee.com/uploads/images/2017/0111/001730_0df99d28_852955.png "报表查询模块")
* 基础数据模块
![输入图片说明](https://gitee.com/uploads/images/2017/0108/150646_a7cbb9c9_852955.png "基础数据模块")
* 系统管理模块
![输入图片说明](https://gitee.com/uploads/images/2017/0108/150703_46711f40_852955.png "系统管理模块")
* 零售出库模块
![输入图片说明](https://gitee.com/uploads/images/2017/0623/222506_5a214201_852955.png "零售出库模块")